<?php if ($curLang == 'en') { ?>
<!-- versão em inglês -->  

    <h2 class="title2">Quality Policy</h2>
    <p class="title2">
        We have an advanced quality control system, from raw material to end product
    </p>
    <p>
        To certify the quality of our services, from raw material to end product, we have equipments 
        to perform material-chemical and electrical tests, following all the steps of production 
        process. In 2005, our advanced quality control system allowed us to conquer the  
        <a href="http://www.anatel.gov.br/" target="_blank">Anatel</a> (National Telecommunication 
        Agency) seal of ratification.
    </p>
    <img src="<?php echo $mediaPath; ?>/politica-de-qualidade-1.jpg" />
    <p class="legend">
        Our high-tech equipments are in all manufacturing process.
    </p>
    <p>
        It certifies the quality of Coopersalto products, because Anatel looks at the 
        telecommunications developments in Brazil, bringing to the national territory a modern and 
        efficient infrastructure, being able to offer suitable services and a variety of goods with 
        low prices.
    </p>
    <img src="<?php echo $mediaPath; ?>/politica-de-qualidade-2.jpg" />
    <p class="legend">
        Our Wires and Cables are safely stored in our new shed.
    </p>
    <p>
        Besides, Coopersalto is affiliated to 
        <a href="http://www.unisolbrasil.org.br/" target="_blank">Unisol</a> - Co-operative society 
        group and mutual undertaking, which joins several corporations and community groups to 
        try to turn the Brazilian society into a fair society toward democracy.
    </p>
    <p>
        <strong>Here there are the main points of our Quality Policy:</strong>
    </p>
    <ul>
        <li>Standard of quality in products and services;</li>
        <li>Standard in serve and technique assistance;</li>
        <li>Competitive market;</li>
        <li>Continual improving in process and products.</li>
    </ul>

<?php } else { ?>
<!-- versão em português --> 

    <h2 class="title2">Política de Qualidade</h2>
    <p class="title2">
        Possuímos um avançado sistema de controle qualidade, <br />da matéria-prima ao produto final
    </p>
    <p>
        Para garantir a qualidade de nossos serviços, da matéria-prima ao produto final, possuímos 
        equipamentos de testes físico-químicos e elétricos que acompanham todas as etapas do processo 
        de fabricação. Esse avançado sistema de controle de qualidade rendeu à Coopersalto no ano de 
        2005, o selo de homologação <a href="http://www.anatel.gov.br/" target="_blank">Anatel</a>
        - Agência Nacional de Telecomunicações.
    </p>
    <img src="<?php echo $mediaPath; ?>/politica-de-qualidade-1.jpg" />
    <p class="legend">
        Os avançados equipamentos da cooperativa acompanham todo o processo de fabricação dos produtos.
    </p>
    <p>
        Essa certificação garante a qualidade dos produtos Coopersalto, já que a Anatel visa promover 
        o desenvolvimento das telecomunicações do País de modo a dotá-lo de uma moderna e eficiente 
        infra-estrutura de telecomunicações, capaz de oferecer à sociedade serviços adequados, 
        diversificados e a preços justos, em todo o território nacional.
    </p>
    <img src="<?php echo $mediaPath; ?>/politica-de-qualidade-2.jpg" />
    <p class="legend">
        A armazenagem dos Fios e Cabos Telefônicos é realizada com total segurança em nosso novo galpão.
    </p>
    <p>
        Além disso, a Coopersalto é filiada a 
        <a href="http://www.unisolbrasil.org.br/" target="_blank">Unisol</a> - Central de Cooperativas 
        e Empreendimentos Solidários, que reúne diversas entidades e empresas coletivas constituídas 
        por trabalhadores e outras modalidades de pessoas jurídicas, e busca o engajamento no processo 
        de transformação da sociedade brasileira em direção à democracia e a uma sociedade mais justa.
    </p>
    <p>
        <strong>Abaixo os principais pontos de nossa Política de Qualidade:</strong>
    </p>
    <ul>
        <li>Padrão de qualidade de produtos e serviços;</li>
        <li>Padrão de atendimento e assistência técnica;</li>
        <li>Condições competitivas de mercado;</li>
        <li>Melhoria contínua nos processos e produtos.</li>
    </ul>

<?php } ?>