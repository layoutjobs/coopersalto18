<?php if ($curLang == 'en') { ?>
<!-- versão em inglês -->  

    <h2 class="title2">Coopersalto</h2>
    <p class="title2">
        We'll let you know a little more about our history and our goals
    </p>
    <img src="<?php echo $mediaPath; ?>/a-coopersalto-1.jpg" />
    <p class="legend">Modern equipment is used in the manufacture of cables.</p>
    <p>
        On April 27, 2002, a group of 110 workers founded the Production Co-operative society of 
        Metallurgist of Salto - Coopersalto. Since then our workers started a bold project to 
        generate work and income to their families, manufacturing products keeping technology 
        and quality. Coopersalto is placed in an area of 6.000 square meters and we 
        work in fixed shifts.
    </p>
    <p>
        <strong>Technology</strong><br>
        The technology used in the production of Wires and Telephonic Cables by Coopersalto is one 
        of the most advanced in Brazil. Our output is 720 thousand kilometers a year and there are 
        two important differentials: Supplying in low quantity and a short time of delivery.
    </p>
    <img src="<?php echo $mediaPath; ?>/a-coopersalto-2.jpg" />
    <p class="legend">Factory production capacity is 720 thousand kilometers pair / year.<p>
    <p>
        <strong>Purpose of our products</strong>
    </p>
    <ul>
        <li>
            Aerial or subterranean grid in ducts;
        </li>
        <li>
            Indoor installations in companies, buildings and telephone exchange;
        </li>
        <li>
            Distribution of telephonic equipments of commutation and terminal distribution.
        </li>
    </ul>
    <img src="<?php echo $mediaPath; ?>/a-coopersalto-4.jpg" />
    <p class="legend">
        Coopersalto provides Wires and Cables in retail and figures on a quick delivery.
    </p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8165.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8119.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8121.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8129.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-cooper-salto-img_8141.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8151.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8166.jpg" /></p>
    <p>
        <strong>Our View</strong><br>
        Practice good prices, fast delivery, excellent service to our customers, always seeking continuous improvement in order to comply the whole domestic market.
    </p>
    <p>
        <strong>Our Mission</strong><br>
        The Cooperative's mission is to commercialize wires and telephone cables with excellent quality in order to ensure the comfort and safety of our customers.
    </p>
    <p>
        <strong>Our Values</strong><br>
        We advocate and practice integrity and ethics.<br>
        We always seek excellence, continuous improvement and innovation.<br>
        Guaranteed Management, Policies and Processes aligned with our values.<br>
        We are the memory and the Cooperative information.<br>
        We practice social and environmental responsibility.
    </p>

    <p>
    <strong>Code of Ethics</strong><br>
    Our day-to-day in the Cooperative shall be guided by the pursuit of quality and efficiency for the results we<br>
    achieve that challenge us. The Coopersalto believes that this path must be followed with a commitment to value<br> 
    and practice-based ethics and compliance, when we relate to different audiences (internal and external) who<br>
    interact with us conduct.
    </p>
    
<?php } else { ?>
<!-- versão em português --> 

    <h2 class="title2">Parceiros</h2>
    <p class="title2">
        Conheça os Parceiros da Coopersalto
    </p>
    
    <br>
    <p style="font-weight: bold;" class="title2">SUDESTE</p>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/plantec.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Plantec</p>
    <p class="tel">(11) 2147-3200</p>
    <hr>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/policom.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Policom</p>
    <p class="tel">(11) 2065-0800</p>
    <hr>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/telcabos.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Telcabos</p>
    <p class="tel">(11) 2146-7777</p>
    <hr>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/antele.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Lantele</p>
    <p class="tel">(21) 3526-5656</p>
    <hr>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="g-tel"  src="<?php echo $mediaPath; ?>/gtel.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">GTEL</p>
    <p class="tel">(11)2333-6530</p>

    <!-- <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/lojaeletricaltda.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Loja Elétrica</p>
    <p class="tel">(31) 3218-8383</p> -->
    <hr>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/janeled.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Jane e Led</p>
    <p class="tel">(21) 3526-5656</p>
    <br>
    <br>
    <p style="font-weight: bold;" class="title2">SUL</p>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/davesat.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Devesat</p>
    <p class="tel">(48) 3626-7879</p>
    <hr>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/dicomp.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Dicomp</p>
    <p class="tel">(44) 4009-2826</p>
    <hr>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/bmconectividade.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">BM Eletro</p>
    <p class="tel">(51) 3346-4400</p>
    <hr>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="oriohm"  src="<?php echo $mediaPath; ?>/oriohm.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Oriohm</p>
    <p class="tel">(47) 3366-3755</p>
    <hr>
    <br>
    <br>
    <p style="font-weight: bold;" class="title2">NORDESTE</p>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/telecabos.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Telecabos</p>
    <p class="tel">(71) 3313-8509</p>
    <hr>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/recicabos.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Recicabos</p>
    <p class="tel">(81) 3225-8550</p>
    <hr>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/lemos-telecom.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Lemos Telecom</p>
    <p class="tel">(81) 3228-0919</p>
    <hr>
    <br>
    <br>
    <p style="font-weight: bold;" class="title2">CENTRO OESTE</p>
    <table class="parceiros" width="100%" height="100%">
        <tr>
            <td><img id="img-parceiros"  src="<?php echo $mediaPath; ?>/dicorel.png" /></td>
        </tr>
    </table>
    <p class="nome-parceiros">Dicorel</p>
    <p class="tel">(67) 3345-2800</p>
    <hr>

<?php } ?>