<?php if ($curLang == 'en') { ?>
<!-- versão em inglês -->  
	<div id="container-content">
		<div class="block">
			<div class="anatel-home"></div>
			 <h3>Products legal ratified by Anatel</h3>
                <p>
                    Our advanced quality control system allowed us to conquer the Anatel seal of ratification...
                </p>
        </div>
		<div class="block">
			<div class="iso"></div>
				<h3>ISO 9001:2015 Certification</h3>
                <p>
                    ISO is a set of rules which presents 
                    requirements to quality management in manufacturing process and services...
                </p>
		</div>
		<div class="block">
			 <div class="ambiental"></div>
               <h3>Respect for the Environment</h3>
                <p>
                    Social, Environmental and Ethical Responsibility, searching for 
                    the reduction of residue and the consumption of polluting material...
                </p>
		</div>
		
		<div class="clearfix"></div>
		
		<div class="block">
	        <a title="Quality Policy" href="<?php echo $basePath; ?>a-coopersalto/politica-de-qualidade.html">Read More</a>
		</div>
		<div class="block">
	        <a title="ISO 9001:2015" href="<?php echo $basePath; ?>a-coopersalto/iso-9001-2015.html">Read More</a>
		</div>
		<div class="block">
	        <a title="Environmental Policy" href="<?php echo $basePath; ?>a-coopersalto/politica-ambiental.html">Read More</a>
		</div>
		
	</div>
	
	
	
    <div class="left">
        <h2 class="title2">Coopersalto presents new Commercial Sales Policy</h2>
        <p>
		Coopersalto presents to its customers a new Commercial Policy for the sale of Telephone Wires and Cables. From now on, our cooperative will only sell from R$ 3,000.00.
		Should any customer need to purchase a small amount, Coopersalto will indicate one of its commercial partners, spread over the South, Sudente, Midwest, North or Northeast regions.
        </p>
    	<img src="<?php echo $mediaPath; ?>/home-1.jpg" />
        <p class="legend">
            We work in permanent shifts and we’re located on an area of 6.000 m².
        </p>
          
    </div>
    <div class="right">
       <h3 class="title2">Stay inside</h3>
			<div class="border-news"></div>
				<p class="title2">Learn more about the latest<br /> technologies and products Coopersalto.</p>
					<div class="border"></div>
					
	   
		<div id="slide-text">   
			<ul class="bxslider">
				<li>
					<h5>Sem mais desculpas para não botar pra fazer! Confira essas dicas valiosíssimas de produtividade.</h5>
					<p>Você sente que nunca tem horas suficientes no seu dia para fazer tudo o que você precisa? A boa notícia é que você não é o único com questões de produtividade. Não sei você, mas eu tenho um monte de pepinos para resolver. Eu costumava ter e-mails brotando igual mato na minha caixa de entrada, uma lista de tarefas quilométrica e aquelas ideias de negócios, uma delas que eu estive pensando em começar por fora há quatro anos, implorando pela minha atenção…</p>
					<p>Ok, esse último eu ainda não consegui resolver, mas pelo menos eu melhorei na parte de controlar o monstro de duas cabeças dos e-mails e do gerenciamento de tarefas. E é assim que você também pode se tornar um ninja da produtividade... <a href="<?php echo $basePath; ?>a-coopersalto/sem-mais-desculpas-para-nao-botar-pra-fazer.html">Continue Lendo &raquo</a></p>
				</li>
				<li>
					<h5>O que é Tecnologia da Informação (TI)?</h5>
					<p>No início, os computadores eram tidos apenas como "máquinas gigantes" que tornavam possível a automatização de determinadas tarefas em instituições de ensino/pesquisa, grandes empresas e nos meios governamentais. Com o avanço tecnológico, tais máquinas começaram a perder espaço para equipamentos cada vez menores, mais poderosos e mais confiáveis.</p>
					<p>Como se não bastasse, a evolução das telecomunicações permitiu que, aos poucos, os computadores passassem a se comunicar, mesmo estando em lugares muito distantes geograficamente... <a href="<?php echo $basePath; ?>a-coopersalto/o-que-e-tecnologia-da-informacao.html">Continue Lendo &raquo</a></p>
				</li>
				<li>
					<h5>Aumente a satisfação do seu cliente com pontualidade nas entregas</h5>
					<p>A falta de pontualidade nas entregas pode ser o fator determinante para a perda de um cliente. Para evitar que isso aconteça, é essencial considerar cada entrega como única. Mas como garantir que a entrega seja feita no tempo previsto? É aí que entra a logística, um diferencial competitivo capaz de aumentar a satisfação do seu cliente.</p>
					<p>O planejamento da entrega deve prever todas as etapas envolvidas no processo: desde tempo para o carregamento do veículo, paradas para abastecimento, almoço, depósito e outros eventos recorrentes. Já para os imprevistos, é importante elaborar um plano de ação que permita a reversão... <a href="<?php echo $basePath; ?>a-coopersalto/aumente-a-satisfacao-do-seu-cliente.html">Continue Lendo &raquo</a></p>
				</li>
				<li>
					<h5>Nova tecnologia atinge velocidade<br>de 10 Gbps usando fios de cobre convencionais</h5>
					<p>Engenheiros de telecomunicações da Bell Labs conseguiram atingir uma taxa de 10 gigabits por segundo utilizando fios de cobre. A nova tecnologia, chamada Dubbed XG.fast, vai permitir estender consideravelmente a vida útil de linhas de fio de cobre, oferecendo uma alternativa mais econômica de modernização que não seja a instalação de fibra ótica onde já existe uma rede de fios de telefone.</p>
					<p>A Bell Labs, agora integrada à gigante de telecomunicações Alcatel-Lucent, desenvolveu a tecnologia XG.fast como uma extensão da G.fast – esta, por sua vez, sucedeu a VDSL2. A nova tecnologia G.fast alcança velocidade de até 1.25 Gbps... <a href="http://canaltech.com.br/noticia/telecom/Nova-tecnologia-atinge-velocidade-de-10-Gbps-em-fios-de-cobre-convencionais/" target="_blank">Continue Lendo &raquo</a></p>
				</li>
				<li>
					<h5>Tim e ZTE se unem para desenvolver<br>a ultra banda larga fixa no Brasil</h5>
					<p>As empresas TIM e ZTE Corporation acabam de assinar um acordo de cooperação estratégica para desenvolver a ultra banda larga fixa. Agora, as duas trabalharão em conjunto para criar novas soluções e tecnologias nesse campo, a fim de fornecer um novo patamar na velocidade de acesso à internet.</p>
					<p>O acordo ocorreu hoje durante o encontro anual do Conselho Empresarial Brasil-China (CEBC). Além das tecnologias conjuntas, teremos a construção de um centro de pesquisa e inovação. O objetivo das instalações será justamente o de testar soluções futuras para a Rede de Acesso do setor.</p>
					<p>A ZTE Brasil, por sua vez, nas palavras de Wang Zhuo, é uma das... <a href="http://www.tudocelular.com/zte/noticias/n38785/tim-e-zte-ultra-banda-larga.html" target="_blank">Continue Lendo &raquo</a></p>
				</li>
				<li>
					<h5>Novas regras beneficiam usuários de telefonia, internet e TV por assinatura</h5>
					<p>Começa a vigorar hoje (8) o Regulamento Geral de Direitos do Consumidor de Serviços de Telecomunicações (RGC), com novas regras a serem seguidas pelas empresas de telefonia, internet e TV por assinatura. Entre os benefícios previstos para os consumidores estão facilidades para o cancelamento imediato de serviços, sem necessidade de falar com atendentes.</p>
					<p>O bloqueio das contas será automático, com prazo máximo de dois dias para conclusão, podendo ser feito por meio de ligação telefônica, pela internet ou pelos terminais. Com o RGC, a Agência Nacional de Telecomunicações (Anatel) busca diminuir o número de reclamações feitas por consumidores à sua... <a href="http://www.ebc.com.br/noticias/brasil/2014/07/novas-regras-beneficiam-usuarios-de-telefonia-internet-e-tv-por-assinatura" target="_blank">Continue Lendo &raquo</a></p>
				</li>
			</ul>
		</div>
		<script>
			$('#slide-text .bxslider').bxSlider({
			  minSlides: 1,
			  maxSlides: 1,
			  moveSlides: 1,
			  slideWidth: 170,
			  slideMargin: 10,
			});
		</script>
	</div>
	
	<div class="clearfix"></div>
	
	<h2 class="productos">Check out our product line:</h2>
	
	<div id="slide-productos">   
		<ul class="bxslider">
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-cce-apl.html"><img src="<?php echo $imagesPath; ?>familia/familia-cce-apl.jpg"  />
				<span>Familia CCE-APL</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-cce-apl-asf.html"><img src="<?php echo $imagesPath; ?>familia/familia-cce-asf.jpg"  />
				<span>familia cce-asf</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-cce-apl-g.html"><img src="<?php echo $imagesPath; ?>familia/familia-cce-g.jpg"  />
				<span>familia cce-apl-g</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-cci.html"><img src="<?php echo $imagesPath; ?>familia/familia-cci.jpg"  />
				<span>Familia CCI</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-cti-pe.html"><img src="<?php echo $imagesPath; ?>familia/familia-cti-pe.jpg" />
				<span>Familia cti-pe</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-ctp-apl.html"><img src="<?php echo $imagesPath; ?>familia/familia-ctp-apl.jpg" />
				<span>Familia ctp-apl</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-ctp-apl-g.html"><img src="<?php echo $imagesPath; ?>familia/familia-ctp-apl-g.jpg" />
				<span>Familia ctp-apl-g</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-ctp-apl-sn.html"><img src="<?php echo $imagesPath; ?>familia/familia-ctp-apl-sn.jpg" />
				<span>Familia ctp-apl-sn</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-fdg.html"><img src="<?php echo $imagesPath; ?>familia/familia-fdg.jpg" />
				<span>Familia fdg</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-fi.html"><img src="<?php echo $imagesPath; ?>familia/familia-fi.jpg" />
				<span>Familia fi</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/cabo-lan.html"><img src="<?php echo $imagesPath; ?>familia/familia-lan.jpg" />
				<span>Familia lan</span>
				</a>
			</li>
		</ul>
	</div>
		<script>
			$('#slide-productos .bxslider').bxSlider({
			  minSlides: 3,
			  maxSlides: 4,
			  moveSlides: 1,
			  slideWidth: 170,
			  slideMargin: 10,
			});
		</script>	
		
		<div class="evidence">
			<blockquote>
				We at Coopersalto, truly believe that good business is only those that conform 
			fully to all interested parties.
			</blockquote>
		</div>
    
<?php } else { ?>
<!-- versão em português --> 
	<div id="container-content">
		<div class="block">
				<div class="anatel-home"></div>
				<h3>Homologação Anatel</h3>
                <p>Nosso avançado sistema de controle de qualidade rendeu-nos o selo de homologação Anatel...</p>
        </div>
		<div class="block">
			<div class="iso"></div>
				<h3>Certificação ISO 9001:2015</h3>
                <p>
                    A ISO é um conjunto de normas que 
                    objetiva apresentar requisitos para gestão da qualidade em processos de fabricação
                    e serviços...
                </p>
		</div>
		<div class="block">
			 <div class="ambiental"></div>
                <h3>Respeito ao Meio Ambiente</h3>
                <p>
                    Responsabilidade Social, Ambiental e Ética, através da busca contínua 
                    da redução de resíduos e consumo de materiais poluentes...
                </p>
		</div>
		
		<div class="clearfix"></div>
		
		<div class="block">
            <a title="Política de Qualidade" href="<?php echo $basePath; ?>a-coopersalto/politica-de-qualidade.html">Leia Mais</a>      
		</div>
		<div class="block">
            <a title="ISO 9001:2015" href="<?php echo $basePath; ?>a-coopersalto/iso-9001-2015.html">Leia Mais</a>
		</div>
		<div class="block">
            <a title="Política Ambiental" href="<?php echo $basePath; ?>a-coopersalto/politica-ambiental.html">Leia Mais</a>
		</div>
	</div>
	
    <div class="left">
		<h2 class="title2">Coopersalto apresenta nova Política Comercial de vendas</h2>
		<p>A Coopersalto apresenta aos seus clientes uma nova Política Comercial para a venda de Fios e Cabos Telefônicos. A partir de agora, nossa cooperativa fará apenas vendas a partir de R$ 3.000,00. Caso algum cliente tenha a necessidade de adquirir uma quantia reduzida, a Coopersalto, indicará um de seus parceiros comerciais, espalhados pela região Sul, Sudeste, Centro Oeste, Norte ou Nordeste. A Coopersalto também apresenta uma nova facilidade para o cliente, com um prazo de entrega de 2 a 5 dias, além das possibilidades de pagamentos com prazos de 28,35 e 42 dias.
		</p>
		   
		<h2 class="title2">A tecnologia Coopersalto está <br> entre as mais avançadas do Brasil</h2>
        <p>
		A tecnologia empregada pela Coopersalto na produção de Fios e Cabos Telefônicos está entre as mais avançadas do Brasil. Nossa capacidade produtiva é de 720 mil km par/ano e possuímos dois diferenciais significativos: O fornecimento em pequenas quantidades e prazos reduzidos de entrega.
        </p>
    	
		<img src="<?php echo $mediaPath; ?>/home-1.jpg" />
        <p class="legend">
            Trabalhamos em sistema de turnos fixos e estamos locados em uma área de 6.000m²
        </p>
    	
		    
    </div>
    <div class="right">
       <h3 class="title2">Fique por dentro</h3>
			<div class="border-news"></div>
				<p class="title2">Saiba mais sobre as últimas tecnologias e produtos Coopersalto.</p>
					<div class="border"></div>
					
	   
		<div id="slide-text">   
			<ul class="bxslider">
				<li>
					<h5>Sem mais desculpas para não botar pra fazer! Confira essas dicas valiosíssimas de produtividade.</h5>
					<p>Você sente que nunca tem horas suficientes no seu dia para fazer tudo o que você precisa? A boa notícia é que você não é o único com questões de produtividade. Não sei você, mas eu tenho um monte de pepinos para resolver. Eu costumava ter e-mails brotando igual mato na minha caixa de entrada, uma lista de tarefas quilométrica e aquelas ideias de negócios, uma delas que eu estive pensando em começar por fora há quatro anos, implorando pela minha atenção…</p>
					<p>Ok, esse último eu ainda não consegui resolver, mas pelo menos eu melhorei na parte de controlar o monstro de duas cabeças dos e-mails e do gerenciamento de tarefas... <a href="<?php echo $basePath; ?>a-coopersalto/sem-mais-desculpas-para-nao-botar-pra-fazer.html">Continue Lendo &raquo</a></p>
				</li>
				<li>
					<h5>O que é Tecnologia da Informação (TI)?</h5>
					<p>No início, os computadores eram tidos apenas como "máquinas gigantes" que tornavam possível a automatização de determinadas tarefas em instituições de ensino/pesquisa, grandes empresas e nos meios governamentais. Com o avanço tecnológico, tais máquinas começaram a perder espaço para equipamentos cada vez menores, mais poderosos e mais confiáveis.</p>
					<p>Como se não bastasse, a evolução das telecomunicações permitiu que, aos poucos, os computadores passassem a se comunicar, mesmo estando em lugares muito distantes geograficamente... <a href="<?php echo $basePath; ?>a-coopersalto/o-que-e-tecnologia-da-informacao.html">Continue Lendo &raquo</a></p>
				</li>
				<li>
					<h5>Aumente a satisfação do seu cliente com pontualidade nas entregas</h5>
					<p>A falta de pontualidade nas entregas pode ser o fator determinante para a perda de um cliente. Para evitar que isso aconteça, é essencial considerar cada entrega como única. Mas como garantir que a entrega seja feita no tempo previsto? É aí que entra a logística, um diferencial competitivo capaz de aumentar a satisfação do seu cliente.</p>
					<p>O planejamento da entrega deve prever todas as etapas envolvidas no processo: desde tempo para o carregamento do veículo, paradas para abastecimento, almoço, depósito e outros eventos recorrentes. Já para os imprevistos, é importante elaborar um plano de ação que permita a reversão... <a href="<?php echo $basePath; ?>a-coopersalto/aumente-a-satisfacao-do-seu-cliente.html">Continue Lendo &raquo</a></p>
				</li>
				<li>
					<h5>Nova tecnologia atinge velocidade<br>de 10 Gbps usando fios de cobre convencionais</h5>
					<p>Engenheiros de telecomunicações da Bell Labs conseguiram atingir uma taxa de 10 gigabits por segundo utilizando fios de cobre. A nova tecnologia, chamada Dubbed XG.fast, vai permitir estender consideravelmente a vida útil de linhas de fio de cobre, oferecendo uma alternativa mais econômica de modernização que não seja a instalação de fibra ótica onde já existe uma rede de fios de telefone.</p>
					<p>A Bell Labs, agora integrada à gigante de telecomunicações Alcatel-Lucent, desenvolveu a tecnologia XG.fast como uma extensão da G.fast – esta, por sua vez, sucedeu a VDSL2. A nova tecnologia G.fast alcança velocidade de até 1.25 Gbps... <a href="http://canaltech.com.br/noticia/telecom/Nova-tecnologia-atinge-velocidade-de-10-Gbps-em-fios-de-cobre-convencionais/" target="_blank">Continue Lendo &raquo</a></p>
				</li>
				<li>
					<h5>Tim e ZTE se unem para desenvolver<br>a ultra banda larga fixa no Brasil</h5>
					<p>As empresas TIM e ZTE Corporation acabam de assinar um acordo de cooperação estratégica para desenvolver a ultra banda larga fixa. Agora, as duas trabalharão em conjunto para criar novas soluções e tecnologias nesse campo, a fim de fornecer um novo patamar na velocidade de acesso à internet.</p>
					<p>O acordo ocorreu hoje durante o encontro anual do Conselho Empresarial Brasil-China (CEBC). Além das tecnologias conjuntas, teremos a construção de um centro de pesquisa e inovação. O objetivo das instalações será justamente o de testar soluções futuras para a Rede de Acesso do setor.</p>
					<p>A ZTE Brasil, por sua vez, nas palavras de Wang Zhuo, é uma das... <a href="http://www.tudocelular.com/zte/noticias/n38785/tim-e-zte-ultra-banda-larga.html" target="_blank">Continue Lendo &raquo</a></p>
				</li>
				<li>
					<h5>Novas regras beneficiam usuários de telefonia, internet e TV por assinatura</h5>
					<p>Começa a vigorar hoje (8) o Regulamento Geral de Direitos do Consumidor de Serviços de Telecomunicações (RGC), com novas regras a serem seguidas pelas empresas de telefonia, internet e TV por assinatura. Entre os benefícios previstos para os consumidores estão facilidades para o cancelamento imediato de serviços, sem necessidade de falar com atendentes.</p>
					<p>O bloqueio das contas será automático, com prazo máximo de dois dias para conclusão, podendo ser feito por meio de ligação telefônica, pela internet ou pelos terminais. Com o RGC, a Agência Nacional de Telecomunicações (Anatel) busca diminuir o número de reclamações feitas por consumidores à sua... <a href="http://www.ebc.com.br/noticias/brasil/2014/07/novas-regras-beneficiam-usuarios-de-telefonia-internet-e-tv-por-assinatura" target="_blank">Continue Lendo &raquo</a></p>
				</li>
			</ul>
		</div>
		<script>
			$('#slide-text .bxslider').bxSlider({
			  minSlides: 1,
			  maxSlides: 1,
			  moveSlides: 1,
			  slideWidth: 170,
			  slideMargin: 10,
			});
		</script>
	</div>
	
	<div class="clearfix"></div>

		
	<h2 class="productos">Conheça a linha de produtos Coopersalto:</h2>
	
	<div id="slide-productos">   
		<ul class="bxslider">
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-cce-apl.html"><img src="<?php echo $imagesPath; ?>familia/familia-cce-apl.jpg"  />
				<span>Familia CCE-APL</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-cce-apl-asf.html"><img src="<?php echo $imagesPath; ?>familia/familia-cce-asf.jpg"  />
				<span>familia cce-asf</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-cce-apl-g.html"><img src="<?php echo $imagesPath; ?>familia/familia-cce-g.jpg"  />
				<span>familia cce-apl-g</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-cci.html"><img src="<?php echo $imagesPath; ?>familia/familia-cci.jpg"  />
				<span>Familia CCI</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-cti-pe.html"><img src="<?php echo $imagesPath; ?>familia/familia-cti-pe.jpg" />
				<span>Familia cti-pe</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-ctp-apl.html"><img src="<?php echo $imagesPath; ?>familia/familia-ctp-apl.jpg" />
				<span>Familia ctp-apl</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-ctp-apl-g.html"><img src="<?php echo $imagesPath; ?>familia/familia-ctp-apl-g.jpg" />
				<span>Familia ctp-apl-g</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-ctp-apl-sn.html"><img src="<?php echo $imagesPath; ?>familia/familia-ctp-apl-sn.jpg" />
				<span>Familia ctp-apl-sn</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-fdg.html"><img src="<?php echo $imagesPath; ?>familia/familia-fdg.jpg" />
				<span>Familia fdg</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/familia-fi.html"><img src="<?php echo $imagesPath; ?>familia/familia-fi.jpg" />
				<span>Familia fi</span>
				</a>
			</li>
			<li>
				<a href="<?php echo $basePath; ?>produtos/cabo-lan.html"><img src="<?php echo $imagesPath; ?>familia/familia-lan.jpg" />
				<span>Familia lan</span>
				</a>
			</li>
		</ul>
	</div>
		<script>
			$('#slide-productos .bxslider').bxSlider({
			  minSlides: 3,
			  maxSlides: 4,
			  moveSlides: 1,
			  slideWidth: 170,
			  slideMargin: 10,
			});
		</script>	
		
		<div class="evidence">
			<blockquote>
				CONFIANÇA é fundamental. COOPERSALTO, seriedade e 
				flexibilidade. Mais de 10 anos desenvolvendo excelência!
			</blockquote>
		</div>

<?php } ?>

<?php
// POPUP NATAL
// include_once(dirname(__FILE__) . '/../includes/popup-natal.php');
?>

<?php
// POPUP 12 ANOS
// include_once(dirname(__FILE__) . '/../includes/popup-12anos.php');
?>