<?php if ($curLang == 'en') { ?>
<!-- versão em inglês -->  

    <h2 class="title">LAN Cable</h2>
    <p class="title">&nbsp;</p>
    <div class="anatel">
        <p>1453-10-2520</p>
    </div>
    <img src="<?php echo $mediaPath; ?>cabo-lan.png" />
    <h3 class="produtos"><span>Product Particularity</span></h3>
    <p>
        <strong>Fabrication:</strong> 
        Manufactured with the highest standard of quality as features for indoor facilities with characteristics of data transmission up to 100MHZ.
        <br />
        <strong>Put into practice:</strong> 
        Internal installation and Gigabit Ethernet.
        <br />
        <strong>Rule:</strong> 
        EIA/TIA 568B.2.
        <br />
        <strong>Anatel Code:</strong> 
        1453-10-2520.
    </p>
	<p><a target="_blank" href="https://sistemas.anatel.gov.br/mosaico/sch/publicView/listarProdutosHomologados.xhtml">Click here</a>
	to view the certificate of ANATEL.</p>
    <h3 class="produtos"><span>Packing</span></h3>
    <p>
        <strong>Box weight:</strong> 
        500g.
    </p>
    <img src="<?php echo $mediaPath; ?>cabo-lan-embalado.png" width="280" />
    <br />
    <h3 class="produtos"><span>Product Structure</span></h3>
    <img src="<?php echo $mediaPath; ?>cabo-lan-estrutura-en.png" />
    <h3 class="produtos"><span>Technical Specification</span></h3>
    <p>
        <a target="_blank" href="http://www.coopersalto.com.br/english/pdf/tecnicas/lan.pdf">Click here</a> 
        to visualize the dimensional data and electrical features in PDF.
    </p>
    <h3 class="produtos"><span>Wrapped Wire Specification</span></h3>
    <p>
        <a target="_blank" href="http://www.coopersalto.com.br/english/pdf/coroas/coroas.pdf">Click here</a> 
        to visualize the specifications in PDF.
    </p>
    <br />
    <br />
    <p class="adobe">
        To visualize PDF files, you need to have Adobe Reader installed on your computer.
        <a target="_blank" href="http://get.adobe.com/br/reader/">Click here</a> 
        to get the latest version directly from the software manufacturer's website.
    </p>
    
<?php } else { ?>
<!-- versão em português -->

    <h2 class="title">Cabo LAN</h2>
    <p class="title">&nbsp;</p>
    <div class="anatel">
        <p>1453-10-2520</p>
    </div>
    <img src="<?php echo $mediaPath; ?>cabo-lan.png" />
    <h3 class="produtos"><span>Detalhes do Produto</span></h3>
    <p>
        <strong>Construção:</strong> 
        Construído com o mais alto padrão e qualidade, conforme características para
        instalações internas com características de transmissão de dados até 100 MHz.
        <br />
        <strong>Aplicação:</strong> 
        Instalação interna e Gigabit Ethernet.
        <br />
        <strong>Norma Aplicável:</strong> 
        EIA/TIA 568B.2.
        <br />
        <strong>Código Anatel:</strong> 
        1453-10-2520.
    </p>
	<p><a target="_blank" href="https://sistemas.anatel.gov.br/mosaico/sch/publicView/listarProdutosHomologados.xhtml">Clique aqui</a>
	para visualizar o certificado da ANATEL.</p>
    <h3 class="produtos"><span>Embalagem</span></h3>
    <p>
        <strong>Peso da caixa:</strong> 
        500g.
    </p>
    <img src="<?php echo $mediaPath; ?>cabo-lan-embalado.png" width="280" />
    <br />
    <h3 class="produtos"><span>Estrutura do Produto</span></h3>
    <img src="<?php echo $mediaPath; ?>cabo-lan-estrutura.png" />
    <h3 class="produtos"><span>Especificações Técnicas</span></h3>
    <p>
        <a target="_blank" href="http://coopersalto.com.br/pdf/tecnicas/lan.pdf">Clique aqui</a> 
        para visualizar os dados dimensionais e as características elétricas em PDF.
    </p>
    <h3 class="produtos"><span>Especificações de Coroas</span></h3>
    <p>
        <a target="_blank" href="http://coopersalto.com.br/pdf/coroas/coroas.pdf">Clique aqui</a> para 
        visualizar as especificações de coroas em PDF.
    </p>
    <br />
    <br />
    <p class="adobe">
        Para visualizar os arquivos em PDF, você precisará ter o Adobe Reader instalado no seu computador. 
        <a target="_blank" href="http://get.adobe.com/br/reader/">Clique aqui</a> 
        para obter a versão mais recente diretamente do site do fabricante do software.
    </p>

<?php } ?>
