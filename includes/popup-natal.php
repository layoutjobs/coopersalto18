<div id="popup-natal" class="modal hide">
	<div class="inner">
		<img src="<?php echo $imagesPath; ?>/popup-natal.jpg" />
		<a href="#" class="close">x</a>
	</div>
</div>

<style>
.hide {
	display: none;
}

#popup-natal {
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	background: rgba(255,255,255,.5);
	z-index: 999999;
	opacity: 0;
	transition: all .3s linear;
	-webkit-transition: all .3s linear;
	-moz-transition: all .3s linear;
}

#popup-natal.in {
	opacity: 1;
}

#popup-natal .inner {
	position: absolute;
	top: -9999px;
	left: 50%;
	width: 480px;
	height: 600px;
	margin-left: -240px;
	margin-top: -300px;
	transition: all .8s linear;
	-webkit-transition: all .8s linear;
}

#popup-natal.in .inner {
	top: 50%;
}

#popup-natal .inner img {
	max-width: 100%;
	margin: -5px;
	border: 5px solid #eee;
	border-radius: 15px;
}

#popup-natal .close {
	position: absolute;
	right: 6px;
	top: 6px;
	width: 32px;
	height: 32px;
	margin: 0;
	font-size: 22px;
	font-weight: bold;
	color: #999;
	line-height: 1;
	text-align: center;
}

@media (min-height: 750px) {
	#popup-natal .inner {
		width: 600px;
		height: 750px;
		margin-left: -300px;
		margin-top: -375px;
	}	
}
</style>

<script>
$(document).ready(function() {
	$('#popup-natal').removeClass('hide');	
	setTimeout(function() { $('#popup-natal').addClass('in'); }, 1);
});
$('#popup-natal').on('click', function(e) {
	$this = $(this);
	$this.removeClass('in');
	setTimeout(function() { $($this).addClass('hide'); }, 300);
});
</script>

<!-- ...ATÉ AQUI! -->