<div id="popup-12anos" class="modal hide">
	<div class="inner">
		<img src="<?php echo $imagesPath; ?>/popup-12anos.jpg" />
		<a href="#" class="close">x</a>
	</div>
</div>

<style>
.hide {
	display: none;
}

#popup-12anos {
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	background: rgba(0,0,0,.5);
	z-index: 999999;
	opacity: 0;
	transition: all .3s linear;
	-webkit-transition: all .3s linear;
	-moz-transition: all .3s linear;
}

#popup-12anos.in {
	opacity: 1;
}

#popup-12anos .inner {
	position: absolute;
	top: -9999px;
	left: 50%;
	width: 852px;
	height: 429px;
	margin-left: -426px;
	margin-top: -214px;
	transition: all .8s linear;
	-webkit-transition: all .8s linear;
}

#popup-12anos.in .inner {
	top: 50%;
}

#popup-12anos .inner img {
	max-width: 100%;
	height: auto;
}

#popup-12anos .close {
	position: absolute;
	right: 6px;
	top: 6px;
	width: 200px;
	height: 42px;
	margin: 0;
	text-indent: -9999px;
}
</style>

<script>
$(document).ready(function() {
	$('#popup-12anos').removeClass('hide');	
	setTimeout(function() { $('#popup-12anos').addClass('in'); }, 1);
});
$('#popup-12anos').on('click', function(e) {
	$this = $(this);
	$this.removeClass('in');
	setTimeout(function() { $($this).addClass('hide'); }, 300);
});
</script>

<!-- ...ATÉ AQUI! -->