<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo($pageName) ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="<?php echo($pageDescription) ?>" />
<meta name="keywords" content="<?php echo($pageKeywords) ?>" />
<link rel="shortcut icon" href="<?php echo $indexDir; ?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo $indexDir; ?>favicon.ico" type="image/x-icon">

<link href="<?php echo $cssPath; ?>reset.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $cssPath; ?>text.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $cssPath; ?>style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $cssPath; ?>nivo-slider-default.css" rel="stylesheet" type="text/css" />

<!-- jQuery library (served from Google) -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="<?php echo $jsPath; ?>jquery.bxslider.min.js"></script>
<!-- bxSlider CSS file -->
<link href="<?php echo $cssPath; ?>jquery.bxslider.css" rel="stylesheet" />

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700' rel='stylesheet' type='text/css'>

<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->

</head>
<body class="<?php echo $curSession; ?> <?php echo $curLang; ?>">
<div id="wrapper">
    
    <div id="header">
    
    <?php if ($curLang == 'en') { ?>
    <!-- #header - versão em inglês -->    
    
        <div id="logo">
            <a href="<?php echo $basePath; ?>" title="Coopersalto | Manufacturer of Wires and Cables Phone"><img src="<?php echo $imagesPath; ?>logo.png" alt="Coopersalto | Fabricante de Fios e Cabos Telefônicos" /></a>
            <span class="slogan">Quality, Technology and Punctuality.</span>
        </div><!-- end #logo -->
        <div id="utils">
            <ul>
                <li>Phone: <span class="phone">+55 11 4028.9696</span></li>
                <li><a href="<?php echo $indexDir; ?>pt/" title="Versão em Português"><img src="<?php echo $imagesPath; ?>flag-brz.png" width="27" height="19" alt="Versão em Português" /></a></li>   
                <li><a href="<?php echo $indexDir; ?>en/" title="English Version"><img src="<?php echo $imagesPath; ?>flag-usa.png" width="27" height="19" alt="English Version" /></a></li>   
            </ul>
        </div><!-- end #utils -->
		<div id="quick-access">           
            <span>Quick Search:</span>
            <div class="dropdown"><a href="#">Família CCE-APL</a></div>
            <div class="itens">
                <span><a title="Família CCE-APL" href="<?php echo $basePath; ?>produtos/familia-cce-apl.html">Família CCE-APL</a></span>
                <span><a title="Família CCE-APL-ASF" href="<?php echo $basePath; ?>produtos/familia-cce-apl-asf.html">Família CCE-APL-ASF</a></span>                        
                <span><a title="Família CCE-APL-G" href="<?php echo $basePath; ?>produtos/familia-cce-apl-g.html">Família CCE-APL-G</a></span>                        
                <span><a title="Família CCI" href="<?php echo $basePath; ?>produtos/familia-cci.html">Família CCI</a></span>                        
                <span><a title="Família CTI" href="<?php echo $basePath; ?>produtos/familia-cti-pe.html">Família CTI-PE</a></li>                        
                <span><a title="Família CTP-APL" href="<?php echo $basePath; ?>produtos/familia-ctp-apl.html">Família CTP-APL</a></span>                        
                <span><a title="Família CTP-APL-G" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-g.html">Família CTP-APL-G</a></span>                        
                <span><a title="Família CTP-APL-SN" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-sn.html">Família CTP-APL-SN</a></span>                        
                <span><a title="Família FDG" href="<?php echo $basePath; ?>produtos/familia-fdg.html">Família FDG</a></span>                        
                <span><a title="Família FI" href="<?php echo $basePath; ?>produtos/familia-fi.html">Família FI</a></span>                        
                <span><a title="Lan Cable" href="<?php echo $basePath; ?>produtos/cabo-lan.html">Lan Cable</a></span>                        
            </div>  
        </div>

         <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style ">
            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
            <a class="addthis_button_tweet"></a>
            <a class="addthis_button_google_plusone" g:plusone:size="medium"></a> 
        </div>
        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
        <script type="text/javascript" src="<?php echo $jsPath; ?>addthis_widget.js"></script>
        <!-- AddThis Button END -->

        <div id="nav"> 
            <ul>
                <li class="first <?php if(($curPage == 'home') || ($curSession == 'home')) echo 'current'; ?>"><a title="Go to home page" href="<?php echo $basePath; ?>">Home</a></li>
                <li class="a-co <?php if(($curPage == 'a-coopersalto') || ($curSession == 'a-coopersalto')) echo 'current'; ?>"><a title="Coopersalto" href="<?php echo $basePath; ?>a-coopersalto.html">Coopersalto <img class="nav-seta" src="<?php echo $imagesPath; ?>nav-seta.png" /></a>
                    <ul>
                        <li><a title="About Us" href="<?php echo $basePath; ?>a-coopersalto/quem-somos.html">About Us</a></li>
                        <li><a title="Quality Policy" href="<?php echo $basePath; ?>a-coopersalto/politica-de-qualidade.html">Quality Policy</a></li>
                        <li><a title="Environmental Policy" href="<?php echo $basePath; ?>a-coopersalto/politica-ambiental.html">Environmental Policy</a></li>
                        <li><a title="ISO 9001:2015" href="<?php echo $basePath; ?>a-coopersalto/iso-9001-2015.html">ISO 9001:2015</a></li>
                        <li><a title="Terms of Sale" href="<?php echo $basePath; ?>a-coopersalto/termos-de-venda.html">Terms of Sale</a></li>
                    </ul>                  
                </li>
                <li class="prod <?php if(($curPage == 'produtos') || ($curSession == 'produtos')) echo 'current'; ?>"><a title="Products" href="<?php echo $basePath; ?>produtos.html">Products <img class="nav-seta" src="<?php echo $imagesPath; ?>nav-seta.png" /></a>
                    <ul>
                        <li><a title="Família CCE-APL" href="<?php echo $basePath; ?>produtos/familia-cce-apl.html">Família CCE-APL</a></li>
                        <li><a title="Família CCE-APL-ASF" href="<?php echo $basePath; ?>produtos/familia-cce-apl-asf.html">Família CCE-APL-ASF</a></li>                        
                        <li><a title="Família CCE-APL-G" href="<?php echo $basePath; ?>produtos/familia-cce-apl-g.html">Família CCE-APL-G</a></li>                        
                        <li><a title="Família CCI" href="<?php echo $basePath; ?>produtos/familia-cci.html">Família CCI</a></li>                        
                        <li><a title="Família CTI-PE" href="<?php echo $basePath; ?>produtos/familia-cti-pe.html">Família CTI-PE</a></li>                        
                        <li><a title="Família CTP-APL" href="<?php echo $basePath; ?>produtos/familia-ctp-apl.html">Família CTP-APL</a></li>                        
                        <li><a title="Família CTP-APL-G" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-g.html">Família CTP-APL-G</a></li>                        
                        <li><a title="Família CTP-APL-SN" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-sn.html">Família CTP-APL-SN</a></li>                        
                        <li><a title="Família FDG" href="<?php echo $basePath; ?>produtos/familia-fdg.html">Família FDG</a></li>                        
                        <li><a title="Família FI" href="<?php echo $basePath; ?>produtos/familia-fi.html">Família FI</a></li>                        
                        <li><a title="LAN Cable" href="<?php echo $basePath; ?>produtos/cabo-lan.html">LAN Cable</a></li>                        
                    </ul>                
                </li>
                <li class="last <?php if(($curPage == 'contato') || ($curSession == 'contato')) echo 'current'; ?>"><a  title="Contact Us" href="<?php echo $basePath; ?>contato.html">Contact Us</a></li>
            </ul>        
            

            <script type="text/javascript">
            	$(document).ready(function () {
                $('.dropdown a').click(function () {
            	$('div.itens').slideToggle('fast');
                });
            });
            </script>
           
        </div><!-- end #nav -->

    <?php } else { ?>
    <!-- #header - versão em português -->    

        <div id="logo">
            <a href="<?php echo $basePath; ?>" title="Coopersalto | Fabricante de Fios e Cabos Telefônicos"><img src="<?php echo $imagesPath; ?>logo.png" alt="Coopersalto | Fabricante de Fios e Cabos Telefônicos" /></a>
            <span class="slogan">Qualidade, Tecnologia e Pontualidade.</span>
        </div><!-- end #logo -->
        <div id="utils">
            <ul>
                <li>Atendimento: <span class="phone">+55 11 4028.9696</span></li>
                <li><a href="<?php echo $indexDir; ?>pt/" title="Versão em Português"><img src="<?php echo $imagesPath; ?>flag-brz.png" width="27" height="19" alt="Versão em Português" /></a></li>   
                <li><a href="<?php echo $indexDir; ?>en/" title="English Version"><img src="<?php echo $imagesPath; ?>flag-usa.png" width="27" height="19" alt="English Version" /></a></li>   
            </ul>
        </div><!-- end #utils -->
		<div id="quick-access">
            <span>Busca Rápida:</span>
		     <div class="dropdown"><a href="#">Família CCE-APL</a></div>
                <div class="itens">
                    <span><a title="Família CCE-APL" href="<?php echo $basePath; ?>produtos/familia-cce-apl.html">Família CCE-APL</a></span>
                    <span><a title="Família CCE-APL-ASF" href="<?php echo $basePath; ?>produtos/familia-cce-apl-asf.html">Família CCE-APL-ASF</a></span>                        
                    <span><a title="Família CCE-APL-G" href="<?php echo $basePath; ?>produtos/familia-cce-apl-g.html">Família CCE-APL-G</a></span>                        
                    <span><a title="Família CCI" href="<?php echo $basePath; ?>produtos/familia-cci.html">Família CCI</a></span>                        
                    <span><a title="Família CTI-PE" href="<?php echo $basePath; ?>produtos/familia-cti-pe.html">Família CTI-PE</a></li>                        
                    <span><a title="Família CTP-APL" href="<?php echo $basePath; ?>produtos/familia-ctp-apl.html">Família CTP-APL</a></span>                        
                    <span><a title="Família CTP-APL-G" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-g.html">Família CTP-APL-G</a></span>                        
                    <span><a title="Família CTP-APL-SN" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-sn.html">Família CTP-APL-SN</a></span>                        
                    <span><a title="Família FDG" href="<?php echo $basePath; ?>produtos/familia-fdg.html">Família FDG</a></span>                        
                    <span><a title="Família FI" href="<?php echo $basePath; ?>produtos/familia-fi.html">Família FI</a></span>                        
                    <span><a title="Cabo LAN" href="<?php echo $basePath; ?>produtos/cabo-lan.html">Cabo LAN</a></span>                        
                </div>  
         </div>
		 
		 <!-- AddThis Button BEGIN -->
		<div class="addthis_toolbox addthis_default_style ">
			<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
			<a class="addthis_button_tweet"></a>
			<a class="addthis_button_google_plusone" g:plusone:size="medium"></a> 
		</div>
		<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
		<script type="text/javascript" src="<?php echo $jsPath; ?>addthis_widget.js"></script>
		<!-- AddThis Button END -->
			
        <div id="nav"> 
            <ul>
                <li class="first <?php if(($curPage == 'home') || ($curSession == 'home')) echo 'current'; ?>"><a title="Ir para a página inicial" href="<?php echo $basePath; ?>">Home</a></li>
                <li class="a-co <?php if(($curPage == 'a-coopersalto') || ($curSession == 'a-coopersalto')) echo 'current'; ?>"><a title="A Coopersalto" href="<?php echo $basePath; ?>a-coopersalto.html">A Coopersalto <img class="nav-seta" src="<?php echo $imagesPath; ?>nav-seta.png" /></a>
                    <ul>
                        <li><a title="Quem Somos" href="<?php echo $basePath; ?>a-coopersalto/quem-somos.html">Quem Somos</a></li>
                        <!-- <li><a title="Parceiros" href="<?php echo $basePath; ?>a-coopersalto/parceiros.html">Parceiros</a></li> -->
                        <li><a title="Política de Qualidade" href="<?php echo $basePath; ?>a-coopersalto/politica-de-qualidade.html">Política de Qualidade</a></li>
                        <li><a title="Política Ambiental" href="<?php echo $basePath; ?>a-coopersalto/politica-ambiental.html">Política Ambiental</a></li>
                        <li><a title="ISO 9001:2015" href="<?php echo $basePath; ?>a-coopersalto/iso-9001-2015.html">ISO 9001:2015</a></li>
                        <li><a title="Termos de Vendas" href="<?php echo $basePath; ?>a-coopersalto/termos-de-venda.html">Termos de Venda</a></li>
                    </ul>                  
                </li>
                <li class="prod <?php if(($curPage == 'produtos') || ($curSession == 'produtos')) echo 'current'; ?>"><a title="Produtos" href="<?php echo $basePath; ?>produtos.html">Produtos <img class="nav-seta" src="<?php echo $imagesPath; ?>nav-seta.png" /></a>
                    <ul>
                        <li><a title="Família CCE-APL" href="<?php echo $basePath; ?>produtos/familia-cce-apl.html">Família CCE-APL</a></li>
                        <li><a title="Família CCE-APL-ASF" href="<?php echo $basePath; ?>produtos/familia-cce-apl-asf.html">Família CCE-APL-ASF</a></li>                        
                        <li><a title="Família CCE-APL-G" href="<?php echo $basePath; ?>produtos/familia-cce-apl-g.html">Família CCE-APL-G</a></li>                        
                        <li><a title="Família CCI" href="<?php echo $basePath; ?>produtos/familia-cci.html">Família CCI</a></li>                        
                        <li><a title="Família CTI-PE" href="<?php echo $basePath; ?>produtos/familia-cti-pe.html">Família CTI-PE</a></li>                        
                        <li><a title="Família CTP-APL" href="<?php echo $basePath; ?>produtos/familia-ctp-apl.html">Família CTP-APL</a></li>                        
                        <li><a title="Família CTP-APL-G" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-g.html">Família CTP-APL-G</a></li>                        
                        <li><a title="Família CTP-APL-SN" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-sn.html">Família CTP-APL-SN</a></li>                        
                        <li><a title="Família FDG" href="<?php echo $basePath; ?>produtos/familia-fdg.html">Família FDG</a></li>                        
                        <li><a title="Família FI" href="<?php echo $basePath; ?>produtos/familia-fi.html">Família FI</a></li>                        
                        <li><a title="Cabo LAN" href="<?php echo $basePath; ?>produtos/cabo-lan.html">Cabo LAN</a></li>                      
                    </ul>                
                </li>
                <li class="last <?php if(($curPage == 'contato') || ($curSession == 'contato')) echo 'current'; ?>"><a  title="Fale Conosco" href="<?php echo $basePath; ?>contato.html">Contato</a></li>
            </ul>        
            

            <script type="text/javascript">
            	$(document).ready(function () {
                $('.dropdown a').click(function () {
            	$('div.itens').slideToggle('fast');
                });
            });
            </script>
           
        </div><!-- end #nav -->

    <?php } ?>        
       <div class="bg-top"></div> 
	</div><!-- end #header -->
	
	<?php if ($curPage == 'home') { ?>    

	    <div id="banner">
		
	        <div class="l1">
	            <div class="l2">
			        <div class="slider-wrapper theme-default">
			            <div id="slider" class="nivoSlider">

                            <?php if ($curLang == 'en') { ?>
                            <!-- <img src="<?php echo $imagesPath; ?>/banner_site_15anos_ingles.jpg" alt="" /> -->
                            <img src="<?php echo $imagesPath; ?>/banner-home-1-en.jpg" alt="" />
                            <img src="<?php echo $imagesPath; ?>/banner-home-2-en.jpg" alt="" />
                            <img src="<?php echo $imagesPath; ?>/banner-home-3-en.jpg" alt="" />
                            <?php } else { ?>
                            <!-- <img src="<?php echo $imagesPath; ?>/banner_site_15anos.jpg" alt="" /> -->
                            <img src="<?php echo $imagesPath; ?>/banner-home.jpg" alt="" />
                            <img src="<?php echo $imagesPath; ?>/banner-home-1.jpg" alt="" />
                            <img src="<?php echo $imagesPath; ?>/banner-home-2.jpg" alt="" />
                            <img src="<?php echo $imagesPath; ?>/banner-home-3.jpg" alt="" />
                            <?php } ?>
			            </div>
			        </div>             	
	            </div>
	        </div>
	    </div><!-- end #banner -->
	
	    <script type="text/javascript" src="<?php echo $jsPath; ?>/jquery.nivo.slider.pack.js"></script>
	    <script type="text/javascript">
	    $(window).load(function() {
	        $('#slider').nivoSlider({
		        effect: 'random', // Specify sets like: 'fold,fade,sliceDown'
		        slices: 15, // For slice animations
		        boxCols: 8, // For box animations
		        boxRows: 4, // For box animations
		        animSpeed: 700, // Slide transition speed
		        pauseTime: 5000, // How long each slide will show
		        startSlide: 0, // Set starting Slide (0 index)
		        directionNav: false, // Next & Prev navigation
		        directionNavHide: true, // Only show on hover
		        controlNav: true, // 1,2,3... navigation
		        controlNavThumbs: false, // Use thumbnails for Control Nav
		        controlNavThumbsFromRel: false, // Use image rel for thumbs
		        controlNavThumbsSearch: '.jpg', // Replace this with...
		        controlNavThumbsReplace: '_thumb.jpg', // ...this in thumb Image src
		        keyboardNav: false, // Use left & right arrows
		        pauseOnHover: false, // Stop animation while hovering
		        manualAdvance: false, // Force manual transitions
		        captionOpacity: 0.8, // Universal caption opacity
		        prevText: 'Prev', // Prev directionNav text
		        nextText: 'Next', // Next directionNav text
		        randomStart: false, // Start on a random slide
		        beforeChange: function(){}, // Triggers before a slide transition
		        afterChange: function(){}, // Triggers after a slide transition
		        slideshowEnd: function(){}, // Triggers after all slides have been shown
		        lastSlide: function(){}, // Triggers when last slide is shown
		        afterLoad: function(){} // Triggers when slider has loaded
	        });
	    });
	    </script>
	
	<?php } else { ?>  
		
	    <div id="banner">
	        <div class="l1">
	            <div class="l2">        	
	            </div>
	        </div>
	    </div><!-- end #banner -->
	    
	<?php } ?>      
	   
    <div id="content">