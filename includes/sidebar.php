<div id="sidebar"> 

<?php if ($curLang == 'en') { ?>
<!-- #sidebar - versão em inglês -->    

    <?php if(($curPage == 'produtos') || ($curSession == 'produtos')){ ?>
      
        <div class="menu">
            <h2>Product Line</h2>
            <ul>
                <li>
                    <a title="Família CCE-APL" href="<?php echo $basePath; ?>produtos/familia-cce-apl.html">Família CCE-APL</a>
                    <span>Telephonic Cable 2 to 6 pairs</span>
                </li>
                <li>
                    <a title="Família CCE-APL-ASF" href="<?php echo $basePath; ?>produtos/familia-cce-apl-asf.html">Família CCE-APL-ASF</a>
                    <span>Telephonic Cable 2 to 30 pairs</span>
                </li>                        
                <li>
                    <a title="Família CCE-APL-G" href="<?php echo $basePath; ?>produtos/familia-cce-apl-g.html">Família CCE-APL-G</a>
                    <span>Telephonic Cable 2 to 6 pairs</span>    
                </li>                        
                <li>
                    <a title="Família CCI" href="<?php echo $basePath; ?>produtos/familia-cci.html">Família CCI</a>
                    <span>Telephonic Cable 1 to 6 pairs </span>
                </li>                        
                <li>
                    <a title="Família CTI-PE" href="<?php echo $basePath; ?>produtos/familia-cti-pe.html">Família CTI-PE</a>
                    <span>Telephonic Cable until 400 pairs</span>
                </li>                        
                <li>
                    <a title="Família CTP-APL" href="<?php echo $basePath; ?>produtos/familia-ctp-apl.html">Família CTP-APL</a>
                    <span>Telephonic Cable until 400 pairs</span>
                </li>                        
                <li>
                    <a title="Família CTP-APL-G" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-g.html">Família CTP-APL-G</a>
                    <span>Telephonic Cable until 400 pairs</span>
                </li>                        
                <li>
                    <a title="Família CTP-APL-SN" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-sn.html">Família CTP-APL-SN</a>
                    <span>Telephonic Cable until 400 pairs</span>
                </li>                        
                <li>
                    <a title="Família FDG" href="<?php echo $basePath; ?>produtos/familia-fdg.html">Família FDG</a>
                    <span>Telephonic Wire</span>
                </li>                        
                <li>
                    <a title="Família FI" href="<?php echo $basePath; ?>produtos/familia-fi.html">Família FI</a>
                    <span>Telephonic Wire</span>
                </li>                        
                <li>
                    <a title="LAN Cable" href="<?php echo $basePath; ?>produtos/cabo-lan.html">LAN Cable</a>
                    <span class="sumir">4X24AWG CAT. 5E UTP CMX</span>
                </li>                        
            </ul>  
        </div>
        <div class="clearfix"></div>  
          
    <?php } else { if($curPage <> 'home') { ?>
        
        <div class="menu">
            <ul>
				<li><a title="About Us" href="<?php echo $basePath; ?>a-coopersalto/quem-somos.html">About Us</a></li>
                <li><a title="Quality Policy" href="<?php echo $basePath; ?>a-coopersalto/politica-de-qualidade.html">Quality Policy</a></li>
                <li><a title="Environmental Policy" href="<?php echo $basePath; ?>a-coopersalto/politica-ambiental.html">Environmental Policy</a></li>
                <li><a title="ISO 9001:2015" href="<?php echo $basePath; ?>a-coopersalto/iso-9001-2015.html">ISO 9001:2015</a></li>
                <li><a title="Terms of Sale" href="<?php echo $basePath; ?>a-coopersalto/termos-de-venda.html">Terms of Sale</a></li>
			</ul>
        </div>
        <div class="clearfix"></div>
     
    <?php } } ?>
    
 

<?php } else { ?>
<!-- #sidebar - versão em português -->    

    <?php if(($curPage == 'produtos') || ($curSession == 'produtos')){ ?>
      
        <div class="menu">
            <h2>Linha de Produtos</h2>
            <ul>
                <li>
                    <a title="Família CCE-APL" href="<?php echo $basePath; ?>produtos/familia-cce-apl.html">Família CCE-APL</a>
                    <span>Cabo Telefônico de 2 a 6 pares</span>
                </li>
                <li>
                    <a title="Família CCE-APL-ASF" href="<?php echo $basePath; ?>produtos/familia-cce-apl-asf.html">Família CCE-APL-ASF</a>
                    <span>Cabo Telefônico de 2 a 30 pares</span>
                </li>                        
                <li>
                    <a title="Família CCE-APL-G" href="<?php echo $basePath; ?>produtos/familia-cce-apl-g.html">Família CCE-APL-G</a>
                    <span>Cabo Telefônico de 2 a 6 pares</span>    
                </li>                        
                <li>
                    <a title="Família CCI" href="<?php echo $basePath; ?>produtos/familia-cci.html">Família CCI</a>
                    <span>Cabo Telefônico de 1 a 6 pares</span>
                </li>                        
                <li>
                    <a title="Família CTI-PE" href="<?php echo $basePath; ?>produtos/familia-cti-pe.html">Família CTI-PE</a>
                    <span>Cabo Telefônico até 400 pares</span>
                </li>                        
                <li>
                    <a title="Família CTP-APL" href="<?php echo $basePath; ?>produtos/familia-ctp-apl.html">Família CTP-APL</a>
                    <span>Cabo Telefônico até 400 pares</span>
                </li>                        
                <li>
                    <a title="Família CTP-APL-G" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-g.html">Família CTP-APL-G</a>
                    <span>Cabo Telefônico até 400 pares</span>
                </li>                        
                <li>
                    <a title="Família CTP-APL-SN" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-sn.html">Família CTP-APL-SN</a>
                    <span>Cabo Telefônico até 400 pares</span>
                </li>                        
                <li>
                    <a title="Família FDG" href="<?php echo $basePath; ?>produtos/familia-fdg.html">Família FDG</a>
                    <span>Fio Telefônico</span>
                </li>                        
                <li>
                    <a title="Família FI" href="<?php echo $basePath; ?>produtos/familia-fi.html">Família FI</a>
                    <span>Fio Telefônico</span>
                </li>                        
                <li>
                    <a title="Cabo LAN" href="<?php echo $basePath; ?>produtos/cabo-lan.html">Cabo LAN</a>
                    <span class="sumir">4X24AWG CAT. 5E UTP CMX</span>
                </li>                        
            </ul>  
        </div>
        <div class="clearfix"></div>  
          
    <?php } else { if($curPage <> 'home') { ?>
        
        <div class="menu">
            <ul>
                <li><a title="Quem Somos" href="<?php echo $basePath; ?>a-coopersalto/quem-somos.html">Quem Somos</a></li>
                <li><a title="Parceiros" href="<?php echo $basePath; ?>a-coopersalto/parceiros.html">Parceiros</a></li>
                <li><a title="Política de Qualidade" href="<?php echo $basePath; ?>a-coopersalto/politica-de-qualidade.html">Política de Qualidade</a></li>
                <li><a title="Política de Ambiental" href="<?php echo $basePath; ?>a-coopersalto/politica-ambiental.html">Política de Ambiental</a></li>
                <li><a title="ISO 9001:2015" href="<?php echo $basePath; ?>a-coopersalto/iso-9001-2015.html">ISO 9001:2015</a></li>
                <li><a title="Termos de Vendas" href="<?php echo $basePath; ?>a-coopersalto/termos-de-venda.html">Termos de Venda</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
     
    <?php } } ?>

<?php } ?>

<br />

<p><a href="http://coopersalto.com.br/catalogo/" target="_blank">
    <img src="<?php echo $imagesPath; ?>/btn-catalogo-online.png" />
</a></p>
<br /><br />
<p><a href="<?php echo $basePath; ?>politica-de-qualidade.html">
    <img src="<?php echo $imagesPath; ?>/selos-iso.png" /><br />
</a></p>

</div><!-- end #sidebar -->