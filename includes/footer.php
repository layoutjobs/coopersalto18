</div><!-- end #content -->
</div><!-- end #wrapper -->
<div class="clearfix"></div>

<div id="footer">
    <div class="wrap">   
    
    <?php if ($curLang == 'en') { ?>
    <!-- #footer - versão em inglês -->        
    
        <div id="copyright">
            <p class="phone"> +55 11 4028.9696</p>
            <p>© <?php echo date('Y'); ?>. All rights reserved.</p>
        </div>
        <div id="sitemap">
            <div class="block">
            <h3>Coopersalto</h3>
            <ul>
                <li><a title="About Us" href="<?php echo $basePath; ?>a-coopersalto/quem-somos.html">About Us</a></li>
                <li><a title="Quality Policy" href="<?php echo $basePath; ?>a-coopersalto/politica-de-qualidade.html">Quality Policy</a></li>
                <li><a title="Environmental Policy" href="<?php echo $basePath; ?>a-coopersalto/politica-ambiental.html">Environmental Policy</a></li>
                <li><a title="ISO 9001:2015" href="<?php echo $basePath; ?>a-coopersalto/iso-9001-2015.html">ISO 9001:2015</a></li>
                <li><a title="Terms of Sale" href="<?php echo $basePath; ?>a-coopersalto/termos-de-venda.html">Terms of Sale</a></li>
            </ul>
            </div>
            <div class="block">
            <h3>Products</h3>
            <ul>
                <li><a title="Família CCE-APL" href="<?php echo $basePath; ?>produtos/familia-cce-apl.html">Família CCE-APL</a></li>
                <li><a title="Família CCE-APL-ASF" href="<?php echo $basePath; ?>produtos/familia-cce-apl-asf.html">Família CCE-APL-ASF</a></li>                        
                <li><a title="Família CCE-APL-G" href="<?php echo $basePath; ?>produtos/familia-cce-apl-g.html">Família CCE-APL-G</a></li>                        
                <li><a title="Família CCI" href="<?php echo $basePath; ?>produtos/familia-cci.html">Família CCI</a></li>                        
                <li><a title="Família CTI-PE" href="<?php echo $basePath; ?>produtos/familia-cti-pe.html">Família CTI-PE</a></li>                        
                <li><a title="Família CTP-APL" href="<?php echo $basePath; ?>produtos/familia-ctp-apl.html">Família CTP-APL</a></li>                        
            </ul> 
            </div>
            <div class="block">
            <h3>&nbsp</h3>
            <ul>
                <li><a title="Família CTP-APL-G" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-g.html">Família CTP-APL-G</a></li>                        
                <li><a title="Família CTP-APL-SN" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-sn.html">Família CTP-APL-SN</a></li>                        
                <li><a title="Família FDG" href="<?php echo $basePath; ?>produtos/familia-fdg.html">Família FDG</a></li>                        
                <li><a title="Família FI" href="<?php echo $basePath; ?>produtos/familia-fi.html">Família FI</a></li>                        
                <li><a title="LAN Cable" href="<?php echo $basePath; ?>produtos/cabo-lan.html">LAN Cable</a></li>                                    
            </ul>
            </div>
            <div class="block">
            <h3>Contact Us</h3>
            <ul>
                <li><a title="Contact Us" href="<?php echo $basePath; ?>produtos/contato.html">Contact Us</a></li>                                                          
            </ul>
            </div>
        </div><!-- end #sitemap -->

    <?php } else { ?>
    <!-- #footer - versão em português -->           

        <div id="copyright">
            <p class="phone">+55 11 4028.9696</p>
            <p>© <?php echo date('Y'); ?>. Todos os direitos reservados.</p>
        </div>
        <div id="sitemap">
            <div class="block">
            <h3>A Coopersalto</h3>
            <ul>
                <li><a title="Quem Somos" href="<?php echo $basePath; ?>a-coopersalto/quem-somos.html">Quem Somos</a></li>
                <li><a title="Parceiros" href="<?php echo $basePath; ?>a-coopersalto/parceiros.html">Parceiros</a></li>
                <li><a title="Política de Qualidade" href="<?php echo $basePath; ?>a-coopersalto/politica-de-qualidade.html">Política de Qualidade</a></li>
                <li><a title="Política Ambiental" href="<?php echo $basePath; ?>a-coopersalto/politica-ambiental.html">Política Ambiental</a></li>
                <li><a title="ISO 9001:2015" href="<?php echo $basePath; ?>a-coopersalto/iso-9001-2015.html">ISO 9001:2015</a></li>
                <li><a title="Termos de Vendas" href="<?php echo $basePath; ?>a-coopersalto/termos-de-venda.html">Termos de Venda</a></li>
            </ul>
            </div>
            <div class="block">
            <h3>Produtos</h3>
            <ul>
                <li><a title="Família CCE-APL" href="<?php echo $basePath; ?>produtos/familia-cce-apl.html">Família CCE-APL</a></li>
                <li><a title="Família CCE-APL-ASF" href="<?php echo $basePath; ?>produtos/familia-cce-apl-asf.html">Família CCE-APL-ASF</a></li>                        
                <li><a title="Família CCE-APL-G" href="<?php echo $basePath; ?>produtos/familia-cce-apl-g.html">Família CCE-APL-G</a></li>                        
                <li><a title="Família CCI" href="<?php echo $basePath; ?>produtos/familia-cci.html">Família CCI</a></li>                        
                <li><a title="Família CTI-PE" href="<?php echo $basePath; ?>produtos/familia-cti-pe.html">Família CTI-PE</a></li>                        
                <li><a title="Família CTP-APL" href="<?php echo $basePath; ?>produtos/familia-ctp-apl.html">Família CTP-APL</a></li>                        
            </ul> 
            </div>
            <div class="block">
            <h3>&nbsp</h3>
            <ul>
                <li><a title="Família CTP-APL-G" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-g.html">Família CTP-APL-G</a></li>                        
                <li><a title="Família CTP-APL-SN" href="<?php echo $basePath; ?>produtos/familia-ctp-apl-sn.html">Família CTP-APL-SN</a></li>                        
                <li><a title="Família FDG" href="<?php echo $basePath; ?>produtos/familia-fdg.html">Família FDG</a></li>                        
                <li><a title="Família FI" href="<?php echo $basePath; ?>produtos/familia-fi.html">Família FI</a></li>                        
                <li><a title="Cabo LAN" href="<?php echo $basePath; ?>produtos/cabo-lan.html">Cabo LAN</a></li>                                    
            </ul>
            </div>
            <div class="block">
            <h3>Contato</h3>
            <ul>
                <li><a title="Fale Conosco" href="<?php echo $basePath; ?>produtos/contato.html">Fale Conosco</a></li>                                                          
            </ul>
            </div>
        </div><!-- end #sitemap -->

    <?php } ?>   
    
</div>
<div class="clearfix"></div>
</div><!-- end #footer -->
</body>
</html>